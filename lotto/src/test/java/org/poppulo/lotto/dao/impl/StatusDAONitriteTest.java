package org.poppulo.lotto.dao.impl;

import org.junit.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.modelmapper.ModelMapper;
import org.poppulo.lotto.dao.StatusDAO;
import org.poppulo.lotto.dao.TicketDAO;
import org.poppulo.lotto.dto.TicketDTO;
import org.poppulo.lotto.entity.Line;
import org.poppulo.lotto.entity.Status;
import org.poppulo.lotto.entity.Ticket;
import org.poppulo.lotto.service.impl.StatusServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class StatusDAONitriteTest {

	@Mock
	StatusDAO statusDAONitriteMock;

	@Mock
	TicketDAO ticketDAONitriteMock;

	@InjectMocks
	StatusServiceImpl statusService;

	@Rule
	public
	MockitoRule mockitoRule = MockitoJUnit.rule();

	private static ModelMapper modelMapper;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		modelMapper = new ModelMapper();
	}

	@After
	public void after() throws Exception {

	}
	
	@Before
	public void setUp() throws Exception {

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	public void generateStatusTest() {

		Ticket ticket = new Ticket();
		when(ticketDAONitriteMock.getTicket(any(Long.class))).thenReturn(ticket);
		when(statusDAONitriteMock.createStatus(any(Status.class))).thenReturn(new Status());
		assertThat(statusService.generateStatus(1l), is(notNullValue()));
	}

	@Test
	public void getStatusTest() {

		when(statusDAONitriteMock.getStatus(any(Long.class))).thenReturn(new Status());
		assertThat(statusService.getStatus(1l), is(notNullValue()));
	}




}
