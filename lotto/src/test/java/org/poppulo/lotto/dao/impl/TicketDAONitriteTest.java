package org.poppulo.lotto.dao.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.modelmapper.ModelMapper;
import org.poppulo.lotto.dao.TicketDAO;
import org.poppulo.lotto.dto.TicketDTO;
import org.poppulo.lotto.entity.Ticket;
import org.poppulo.lotto.service.impl.TicketServiceImpl;

public class TicketDAONitriteTest {

	@Mock
	TicketDAO ticketDAONitriteMock;

	@InjectMocks
	TicketServiceImpl ticketService;

	@Rule
	public
	MockitoRule mockitoRule = MockitoJUnit.rule();

	private static ModelMapper modelMapper;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		modelMapper = new ModelMapper();
	}

	@After
	public void after() throws Exception {

	}
	
	@Before
	public void setUp() throws Exception {

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	public void testGetTickets() {
		List<Ticket> tickets = new ArrayList<>();
		tickets.add(new Ticket());
		when(ticketDAONitriteMock.getTickets()).thenReturn(tickets);
		assertThat(ticketService.getTickets(), is(notNullValue()));
	}

	@Test
	public void testGetTicket() {

		TicketDTO ticketDTO = new TicketDTO(1l, 2);
		Ticket ticket = modelMapper.map(ticketDTO, Ticket.class);
		when(ticketDAONitriteMock.getTicket(any(Long.class))).thenReturn(ticket);
		TicketDTO createdDTO = ticketService.getTicket(ticketDTO.getTicketID());
		assertEquals(ticketDTO.getTicketID(), createdDTO.getTicketID());
	}

	@Test
	public void testCreateTicket() {

		when(ticketDAONitriteMock.createTicket(any(Ticket.class))).thenReturn(new Ticket());
		assertThat(ticketService.createTicket(new TicketDTO()), is(notNullValue()));
	}

	@Test
	public void testUpdateTicket() {
		when(ticketDAONitriteMock.updateTicket(any(Ticket.class))).thenReturn(new Ticket());
		assertThat(ticketService.updateTicket(new TicketDTO()), is(notNullValue()));
	}

	@Test
	public void testDeleteTicket() {
		ticketService.deleteTicket(1l);
		verify(ticketDAONitriteMock).deleteTicket(any(Long.class));
	}

}
