package org.poppulo.lotto;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.poppulo.lotto.resources.StatusResource;
import org.poppulo.lotto.resources.TicketResource;

/**
 * 
 *
 */
@ApplicationPath("/lotto")
public class App extends Application {
 
    private Set<Object> singletons = new HashSet<Object>();
 
    public App() {
    	// Add REST Resources
        singletons.add(new TicketResource());
        singletons.add(new StatusResource());
    }
 
    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
