package org.poppulo.lotto.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.poppulo.lotto.dao.impl.StatusDAONitrite;
import org.poppulo.lotto.dao.impl.TicketDAONitrite;
import org.poppulo.lotto.dto.TicketDTO;
import org.poppulo.lotto.entity.Status;
import org.poppulo.lotto.entity.Ticket;
import org.poppulo.lotto.service.StatusService;
import org.poppulo.lotto.service.TicketService;
import org.poppulo.lotto.service.impl.StatusServiceImpl;
import org.poppulo.lotto.service.impl.TicketServiceImpl;
import org.poppulo.lotto.persist.DatabaseManager;

import java.util.List;

@Path("/ticket")
public class TicketResource {

	@GET
	@Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getByID(@PathParam("id") Long ticketID) {
		try {
			TicketService ticketService = getTicketService();
			TicketDTO ticketDTO = ticketService.getTicket(ticketID);
			return Response.status(Response.Status.OK).entity(ticketDTO).build();
		} finally {
			DatabaseManager.getDB().close();
		}
	}

	@GET
	@Path("")
	@Produces({MediaType.APPLICATION_JSON})
	public Response list() {
		try {
			TicketService ticketService = getTicketService();
			List<TicketDTO> ticketDTOList = ticketService.getTickets();
			return Response.status(Response.Status.OK).entity(ticketDTOList).build();
		} finally {
			DatabaseManager.getDB().close();
		}
	}

	@POST
	@Path("/")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response create(TicketDTO ticketDTO,
						   @DefaultValue("2") @QueryParam("numLines") int numLines) {
		try {

			TicketService ticketService = getTicketService();

			if (ticketDTO == null) {
				ticketDTO = new TicketDTO(null, numLines);
			} else if (null != ticketDTO.getTicketID() && null != ticketService.getTicket(ticketDTO.getTicketID())) {

				// Don't process the ticket any further if it has been created already
				return Response.status(Response.Status.NOT_MODIFIED).build();
			}
			ticketDTO = ticketService.createTicket(ticketDTO);
			return Response.status(Response.Status.CREATED).entity(ticketDTO).build();
		} finally {
			DatabaseManager.getDB().close();
		}
	}

	@PUT
	@Path("/{ticketID}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response amend(@PathParam("ticketID") Long ticketID,
						  @DefaultValue("2") @QueryParam("numLines") int numLines) {

		try {
			TicketService ticketService = getTicketService();
			StatusService statusService = getStatusService();

			TicketDTO ticketDTO = ticketService.getTicket(ticketID);
			if (ticketDTO == null) {
				return Response.status(Response.Status.NOT_FOUND).entity("{\"message\": \"Ticket not found\"}").build();
			} else if (ticketDTO != null && statusService.getStatus(ticketID) == null) {
				ticketDTO.getLines().addAll(ticketDTO.generateLines(numLines));
			} else if (statusService.getStatus(ticketID) != null) {
				return Response.status(Response.Status.BAD_REQUEST).entity("{\"message\": \"Amendment of lines is not permitted after status has been checked\"}").build();
			}
			ticketDTO = ticketService.updateTicket(ticketDTO);
			return Response.status(Response.Status.OK).entity(ticketDTO).build();
		} finally {
			DatabaseManager.getDB().close();
		}
	}

	private TicketService getTicketService() {
		return new TicketServiceImpl(new TicketDAONitrite(DatabaseManager
				.getDB().getRepository(Ticket.class)));
	}

	private StatusService getStatusService() {
		return new StatusServiceImpl(new StatusDAONitrite(DatabaseManager
				.getDB().getRepository(Status.class)), new TicketDAONitrite(DatabaseManager
				.getDB().getRepository(Ticket.class)));
	}

}
