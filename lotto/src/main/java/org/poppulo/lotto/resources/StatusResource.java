package org.poppulo.lotto.resources;

import org.poppulo.lotto.dao.impl.StatusDAONitrite;
import org.poppulo.lotto.dao.impl.TicketDAONitrite;
import org.poppulo.lotto.dto.StatusDTO;
import org.poppulo.lotto.entity.Status;
import org.poppulo.lotto.entity.Ticket;
import org.poppulo.lotto.service.StatusService;
import org.poppulo.lotto.service.TicketService;
import org.poppulo.lotto.service.impl.StatusServiceImpl;
import org.poppulo.lotto.service.impl.TicketServiceImpl;
import org.poppulo.lotto.persist.DatabaseManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/status")
public class StatusResource {


	@PUT
	@Path("/{ticketID}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response checkStatus(
			@PathParam("ticketID") Long ticketID) {


		try {
			StatusService statusService = getStatusService();
			TicketService ticketService = getTicketService();
			StatusDTO statusDTO = statusService.getStatus(ticketID);
			if (ticketService.getTicket(ticketID) == null) {
				return Response.status(Response.Status.NOT_FOUND).entity("{\"message\": \"Ticket not found\"}").build();
			}
			if (statusDTO == null) {
				statusDTO = statusService.generateStatus(ticketID);
			}

			return Response.status(Response.Status.OK).entity(statusDTO).build();

		} finally {
			DatabaseManager.getDB().close();
		}

	}

	private TicketService getTicketService() {
		return new TicketServiceImpl(new TicketDAONitrite(DatabaseManager
				.getDB().getRepository(Ticket.class)));
	}

	private StatusService getStatusService() {
		return new StatusServiceImpl(new StatusDAONitrite(DatabaseManager
				.getDB().getRepository(Status.class)), new TicketDAONitrite(DatabaseManager
				.getDB().getRepository(Ticket.class)));
	}

}
