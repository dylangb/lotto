package org.poppulo.lotto.persist;

import org.dizitart.no2.Nitrite;

/**
 * Manages access to the Nitrite DB
 */
public class DatabaseManager {
	
	public static String DB_PATH = "";
	private static Nitrite db = null;
	
	
	public static Nitrite getDB() {
		if (null == db || db.isClosed()) {
			db = Nitrite.builder()
			        .filePath(DB_PATH)
			        .openOrCreate();
		} 
		
		return db;
	}

}
