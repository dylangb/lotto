package org.poppulo.lotto.entity;

/**
 * Represents a line of numbers on a lotto Ticket
 */
public class Line {

	private int[] numList;
	
	public Line(int[] numList) {
		this.numList = numList;
	}
	
	public Line() {
		
	}

	public int[] getNumList() {
		return numList;
	}

	public void setNumList(int[] numList) {
		this.numList = numList;
	}

	
}
