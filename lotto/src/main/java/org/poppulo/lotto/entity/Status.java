package org.poppulo.lotto.entity;

import java.util.List;
import java.util.Map;

/**
 * Object representing the outcomes calculated by applying certain rules to a Ticket's list of lines
 */
public class Status {

	private Long statusID;
	private Map<Integer, List<Line>> outcomes;

	public Status() {

	}

	public Status(Map<Integer, List<Line>> outcomes) {
		this.outcomes = outcomes;
	}

	public Map<Integer, List<Line>> getOutcomes() {
		return outcomes;
	}

	public void setOutcomes(Map<Integer, List<Line>> outcomes) {
		this.outcomes = outcomes;
	}

	public Long getStatusID() {
		return statusID;
	}

	public void setStatusID(Long statusID) {
		this.statusID = statusID;
	}
}
