package org.poppulo.lotto.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.dizitart.no2.objects.Id;

/**
 * A lotto Ticket
 */
public class Ticket {

	@Id
	private Long ticketID;

	private List<Line> lines = new ArrayList<Line>();
	
	public Ticket() {

	}
	
	public Ticket(Long ticketID, int numLines) {
		this.ticketID = ticketID;

	}



	public List<Line> getLines() {
		return lines;
	}

	public void setLines(List<Line> lines) {
		this.lines = lines;
	}

	public Long getTicketID() {
		return ticketID;
	}

	public void setTicketID(Long ticketID) {
		this.ticketID = ticketID;
	}
	

	
}
