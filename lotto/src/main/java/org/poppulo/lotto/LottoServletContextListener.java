package org.poppulo.lotto;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.poppulo.lotto.persist.DatabaseManager;


@WebListener
public class LottoServletContextListener implements ServletContextListener {
	
	 
		@Override
		public void contextDestroyed(ServletContextEvent arg0) {

		}
	 
		//Run this before web application is started
		@Override
		public void contextInitialized(ServletContextEvent contextEvent) {
			final String appPath = contextEvent.getServletContext().getRealPath("");

			// Create Nitrite DB in temporary directory
			String tmpPath = DatabaseManager.DB_PATH = appPath + "tmp";
			new File(tmpPath).mkdirs();
			DatabaseManager.DB_PATH = tmpPath + File.separator + "test.db";
		}

}
