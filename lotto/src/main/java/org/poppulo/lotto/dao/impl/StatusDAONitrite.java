package org.poppulo.lotto.dao.impl;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.dizitart.no2.util.Iterables;
import org.poppulo.lotto.dao.StatusDAO;
import org.poppulo.lotto.entity.Status;

/**
 * NitriteDB implementation of StatusDAO
 */
public class StatusDAONitrite implements StatusDAO {

	private ObjectRepository<Status> repository = null;

	public StatusDAONitrite(ObjectRepository<Status> repository) {
		this.repository = repository;
	}

	@Override
	public Status getStatus(Long statusID) {
		Status status = Iterables.firstOrDefault(
				this.repository.find(
						ObjectFilters.eq("statusID", statusID)));
		return status;
	}

	@Override
	public Status createStatus(Status status) {

		WriteResult writeResult = this.repository.insert(status);
		status = getStatusFromWriteResult(writeResult);

		return status;
	}

	
	private Status getStatusFromWriteResult(WriteResult writeResult) {
		Status status  = null;
		if (writeResult != null) {
			NitriteId nitriteID = Iterables.firstOrDefault(writeResult);
			status  = this.repository.getById(nitriteID);
		}
		
		return status;
	}
	
	public void close(){
		this.repository.close();
	}
	

}
