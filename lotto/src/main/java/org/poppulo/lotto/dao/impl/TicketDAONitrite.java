package org.poppulo.lotto.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.dizitart.no2.util.Iterables;
import org.poppulo.lotto.dao.TicketDAO;
import org.poppulo.lotto.entity.Ticket;

/**
 * NitriteDB implementation of TicketDAO
 */
public class TicketDAONitrite implements TicketDAO {
	
	private ObjectRepository<Ticket> repository = null;
	
	public TicketDAONitrite(ObjectRepository<Ticket> repository) {
		this.repository = repository;
	}

	@Override
	public List<Ticket> getTickets() {
		List<Ticket> ticketList = new ArrayList<Ticket>();
		Cursor<Ticket> tickets = this.repository.find();
		for (Ticket ticket : tickets) {
			ticketList.add(ticket);
		}
		return ticketList;
	}

	@Override
	public Ticket getTicket(Long ticketID) {
		
		Ticket ticket = Iterables.firstOrDefault(
				this.repository.find(
						ObjectFilters.eq("ticketID", ticketID)));
		return ticket;
	}

	@Override
	public Ticket createTicket(Ticket ticket) {
		
		if (ticket != null) {
			ticket.setTicketID(generateID());
		}
		WriteResult writeResult = this.repository.insert(ticket);
		ticket = getTicketFromWriteResult(writeResult);
		
		return ticket;
	}

	@Override
	public Ticket updateTicket(Ticket ticket) {
		WriteResult writeResult = this.repository.update(ticket);
		ticket = getTicketFromWriteResult(writeResult);
		
		return ticket;
	}

	@Override
	public void deleteTicket(Long ticketID) {
		Ticket ticket = this.getTicket(ticketID);
		this.repository.remove(ticket);
		
	}
	
	private Long generateID() {
		return UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
	}
	
	private Ticket getTicketFromWriteResult(WriteResult writeResult) {
		Ticket ticket = null;
		if (writeResult != null) {
			NitriteId nitriteID = Iterables.firstOrDefault(writeResult);
			ticket = this.repository.getById(nitriteID);
		}
		
		return ticket;
	}
	
	public void close(){
		this.repository.close();
	}
	

}
