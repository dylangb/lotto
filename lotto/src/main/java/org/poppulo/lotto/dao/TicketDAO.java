package org.poppulo.lotto.dao;

import org.poppulo.lotto.entity.Ticket;

import java.util.List;

/**
 *
 */
public interface TicketDAO {

    List<Ticket> getTickets();
    Ticket getTicket(Long ticketID);
    Ticket createTicket(Ticket ticket);
    Ticket updateTicket(Ticket ticket);
    void deleteTicket(Long ticketID);
}
