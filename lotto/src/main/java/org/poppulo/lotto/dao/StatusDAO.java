package org.poppulo.lotto.dao;

import org.poppulo.lotto.entity.Status;

public interface StatusDAO {

	Status createStatus(Status ticket);
	Status getStatus(Long statusID);

}
