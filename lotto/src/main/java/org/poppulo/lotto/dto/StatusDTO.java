package org.poppulo.lotto.dto;

import java.util.List;
import java.util.Map;

public class StatusDTO {

    Long statusID;
    Map<Integer, List<LineDTO>> outcomes;

    public StatusDTO() {

    }

    public StatusDTO(Map<Integer, List<LineDTO>> outcomes) {
        this.outcomes = outcomes;
    }

    public Map<Integer, List<LineDTO>> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(Map<Integer, List<LineDTO>> outcomes) {
        this.outcomes = outcomes;
    }

    public Long getStatusID() {
        return statusID;
    }

    public void setStatusID(Long statusID) {
        this.statusID = statusID;
    }
}
