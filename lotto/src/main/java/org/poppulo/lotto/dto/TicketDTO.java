package org.poppulo.lotto.dto;

import org.poppulo.lotto.entity.Line;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TicketDTO {
    private Long ticketID;
    private List<LineDTO> lines = new ArrayList<LineDTO>();

    public TicketDTO() {

    }

    public TicketDTO(Long ticketID, int numLines) {
        this.ticketID = ticketID;
        this.lines = this.generateLines(numLines);
    }

    public Long getTicketID() {
        return ticketID;
    }

    public void setTicketID(Long ticketID) {
        this.ticketID = ticketID;
    }

    public List<LineDTO> getLines() {
        return lines;
    }

    public void setLines(List<LineDTO> lines) {
        this.lines = lines;
    }

    public List<LineDTO> generateLines(int numLines) {
        Random random = new Random();
        List<LineDTO> lines = new ArrayList<LineDTO>();
        for (int i = 0; i < numLines; i++) {
            int[] numList = {random.nextInt(3), random.nextInt(3), random.nextInt(3)};
            LineDTO line = new LineDTO(numList);
            lines.add(line);
        }

        return lines;
    }
}
