package org.poppulo.lotto.dto;

public class LineDTO {

    private int[] numList;

    public LineDTO() {

    }

    public LineDTO(int[] numList) {
        this.numList = numList;
    }

    public int[] getNumList() {
        return numList;
    }

    public void setNumList(int[] numList) {
        this.numList = numList;
    }
}
