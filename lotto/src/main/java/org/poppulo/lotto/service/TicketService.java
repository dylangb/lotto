package org.poppulo.lotto.service;

import org.poppulo.lotto.dto.TicketDTO;

import java.util.List;

/**
 * Service to manage CRUD operations of Tickets
 */
public interface TicketService {

    List<TicketDTO> getTickets();
    TicketDTO getTicket(Long ticketID);
    TicketDTO createTicket(TicketDTO ticketDTO);
    TicketDTO updateTicket(TicketDTO ticketDTO);
    void deleteTicket(Long ticketID);
}
