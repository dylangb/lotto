package org.poppulo.lotto.service.impl;

import org.modelmapper.ModelMapper;
import org.poppulo.lotto.dao.TicketDAO;
import org.poppulo.lotto.dto.TicketDTO;
import org.poppulo.lotto.entity.Ticket;
import org.poppulo.lotto.service.TicketService;

import java.util.List;
import java.util.stream.Collectors;

public class TicketServiceImpl implements TicketService {

    private TicketDAO ticketDAO;
    private ModelMapper modelMapper;

    public TicketServiceImpl(TicketDAO ticketDAO) {
        this.ticketDAO = ticketDAO;
        this.modelMapper = new ModelMapper();
    }

    public TicketDAO getTicketDAO() {
        return ticketDAO;
    }

    public void setTicketDAO(TicketDAO ticketDAO) {
        this.ticketDAO = ticketDAO;
    }

    public ModelMapper getModelMapper() {
        return modelMapper;
    }

    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    private TicketDTO convertToDTO(Ticket ticket) {
        TicketDTO ticketDTO = null;
        if (ticket != null) {
            ticketDTO = this.modelMapper.map(ticket, TicketDTO.class);
        }
        return ticketDTO;
    }

    private Ticket convertToEntity(TicketDTO ticketDTO) {

        return this.modelMapper.map(ticketDTO, Ticket.class);
    }

    @Override
    public List<TicketDTO> getTickets() {
        return this.ticketDAO.getTickets().stream()
                .map(ticket -> convertToDTO(ticket))
                .collect(Collectors.toList());
    }

    @Override
    public TicketDTO getTicket(Long ticketID){
        return convertToDTO(this.ticketDAO.getTicket(ticketID));
    }

    @Override
    public TicketDTO createTicket(TicketDTO ticketDTO) {
        Ticket ticket = convertToEntity(ticketDTO);
        ticket = this.ticketDAO.createTicket(ticket);
        return convertToDTO(ticket);
    }

    @Override
    public TicketDTO updateTicket(TicketDTO ticketDTO) {
        Ticket ticket = convertToEntity(ticketDTO);
        ticket = this.ticketDAO.updateTicket(ticket);
        return convertToDTO(ticket);
    }

    @Override
    public void deleteTicket(Long ticketID) {
        this.ticketDAO.deleteTicket(ticketID);
    }
}
