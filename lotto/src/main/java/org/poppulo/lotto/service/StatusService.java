package org.poppulo.lotto.service;

import org.poppulo.lotto.dto.StatusDTO;

/**
 * Service to manage checking the status of lotto tickets
 */
public interface StatusService {

    /**
     * Retrieves a lotto ticket by ID, processes it into outcomes,
     * and stores the outcomes as a Status object
     *
     * @param ticketID
     * @return
     */
    StatusDTO generateStatus(Long ticketID);

    /**
     * Retrieve an existing Status
     *
     * @param statusID
     * @return
     */
    StatusDTO getStatus(Long statusID);
}
