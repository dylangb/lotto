package org.poppulo.lotto.service.impl;

import org.modelmapper.ModelMapper;
import org.poppulo.lotto.dao.StatusDAO;
import org.poppulo.lotto.dao.TicketDAO;
import org.poppulo.lotto.dto.StatusDTO;
import org.poppulo.lotto.entity.Line;
import org.poppulo.lotto.entity.Status;
import org.poppulo.lotto.entity.Ticket;
import org.poppulo.lotto.service.StatusService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatusServiceImpl implements StatusService {

    private StatusDAO statusDAO;
    private TicketDAO ticketDAO;
    private ModelMapper modelMapper;

    public StatusServiceImpl(StatusDAO statusDAO, TicketDAO ticketDAO) {
        this.statusDAO = statusDAO;
        this.ticketDAO = ticketDAO;
        this.modelMapper = new ModelMapper();
    }

    private StatusDTO convertToDTO(Status status) {
    	StatusDTO statusDTO = null;
    	if (status != null) {
    		statusDTO = this.modelMapper.map(status, StatusDTO.class);
    	}
        return statusDTO;
    }

    @Override
    public StatusDTO generateStatus(Long ticketID) {
        Ticket ticket = ticketDAO.getTicket(ticketID);
        Status status = this.calculateResult(ticket);
        status.setStatusID(ticketID);
        status = this.statusDAO.createStatus(status);
        return convertToDTO(status);
    }

    @Override
    public StatusDTO getStatus(Long statusID) {
        Status status = this.statusDAO.getStatus(statusID);
        return convertToDTO(status);
    }

    private Status calculateResult(Ticket ticket) {
        Map<Integer, List<Line>> outcomes = new HashMap<>();
        Status status = new Status();

        for (Line line : ticket.getLines()) {
            outcomes = processLine(outcomes, line);
        }
        status.setOutcomes(outcomes);
        return status;
    }

    private Map<Integer, List<Line>> processLine(Map<Integer, List<Line>> outcomes, Line line) {


        int first = line.getNumList()[0];
        int second = line.getNumList()[1];
        int third = line.getNumList()[2];

        int result = 0;
        if (first + second + third == 3) {
            result = 10;
        } else if (first == second && second == third) {
            result = 5;
        } else if ((second != first) && (third != first)) {
            result = 1;
        }

        if (outcomes.containsKey(result)) {
            outcomes.get(result).add(line);
        } else {
            outcomes.put(result, new ArrayList<>());
            outcomes.get(result).add(line);
        }

        return outcomes;
    }

}
