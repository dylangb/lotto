package org.poppulo.lotto.rest;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.poppulo.lotto.dto.StatusDTO;
import org.poppulo.lotto.dto.TicketDTO;
import org.poppulo.lotto.integration.IntegrationTest;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Category(IntegrationTest.class)
public class TicketResourceTest {

	private static final String URL = "/lotto/v1/ticket/";
	private static final String STATUS_URL = "/lotto/v1/status/";
	
	@BeforeClass
	public static void setup() {
	}

	@AfterClass
	public static void cleanUp(){
	}

	@Test
	public void shouldSayHelloWorld(){

		assertTrue(true);
	}

	@Test
	public void  create_ticket_gives_201(){

		given().contentType("application/json")
				.body(new TicketDTO(null, 2))
				.when()
				.post(URL)
				.then()
				.statusCode(201);
	}

	@Test
	public void retrieve_all_tickets_should_give_200_status(){

		given()
				.when()
				.get(URL)
				.then()
				.statusCode(200);
	}

	@Test
	public void create_ticket_then_amend(){

		TicketDTO ticketDTO = new TicketDTO(null, 3);
		ticketDTO = given().contentType("application/json")
				.body(ticketDTO)
				.when().post(URL)
				.as(TicketDTO.class);

		ticketDTO = given().queryParam("numLines", 3).pathParam("ticketID", ticketDTO.getTicketID()).when().put(URL + "{ticketID}").as(TicketDTO.class);
		assertEquals(ticketDTO.getLines().size(), 6);

	}

	@Test
	public void create_ticket_then_check_status_then_amend_receive_400_bad_request(){

		TicketDTO ticketDTO = new TicketDTO(null, 3);
		ticketDTO = given().contentType("application/json")
				.body(ticketDTO)
				.when().post(URL)
				.as(TicketDTO.class);

		StatusDTO status =
				given().contentType("application/json").pathParam("ticketID", ticketDTO.getTicketID()).when().put(STATUS_URL + "{ticketID}").as(StatusDTO.class);

		given().pathParam("ticketID", ticketDTO.getTicketID()).when().put(URL + "{ticketID}").then().statusCode(400);

	}

}


