package org.poppulo.lotto.rest;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.poppulo.lotto.dto.LineDTO;
import org.poppulo.lotto.dto.StatusDTO;
import org.poppulo.lotto.dto.TicketDTO;
import org.poppulo.lotto.integration.IntegrationTest;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.*;

@Category(IntegrationTest.class)
public class StatusResourceTest {

	private static final String STATUS_URL = "/lotto/v1/status/";
	private static final String TICKET_URL = "/lotto/v1/ticket/";
	
	@BeforeClass
	public static void setup() {
		RestAssured.defaultParser = Parser.JSON;
	}

	@AfterClass
	public static void cleanUp(){
	}

	@Test
	public void create_ticket_and_verify_status(){

		TicketDTO ticketDTO = generateTestTicket();
		ticketDTO = given()
				.contentType("application/json")
				.body(ticketDTO)
				.when().post(TICKET_URL)
				.as(TicketDTO.class);

		StatusDTO status =
		given().contentType("application/json").pathParam("ticketID", ticketDTO.getTicketID()).when().put(STATUS_URL + "{ticketID}").as(StatusDTO.class);
		assertTrue(status.getOutcomes().get(5).size() == 1);
		assertTrue(status.getOutcomes().get(1).size() == 1);
		assertTrue(status.getOutcomes().get(10).size() == 1);
		assertNull(status.getOutcomes().get(4));
		assertNull(status.getOutcomes().get(0));

	}



	@Test
	public void return_404_for_unknown_ticket(){

		given().expect().statusCode(404).when().put(STATUS_URL + "5");
	}

	private TicketDTO generateTestTicket() {
		TicketDTO ticket = new TicketDTO();

		List<LineDTO> lines = new ArrayList<>();

		int[] numList = {0, 0, 0}; // 5
		LineDTO line = new LineDTO(numList);
		lines.add(line);

		int[] numList2 = {0, 1, 1};
		LineDTO line2 = new LineDTO(numList2);
		lines.add(line2);

		int[] numList3 = {0, 1, 2};
		LineDTO line3 = new LineDTO(numList3);
		lines.add(line3);

		ticket.setLines(lines);
		return ticket;
	}

}


